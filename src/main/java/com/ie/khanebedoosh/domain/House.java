package com.ie.khanebedoosh.domain;
import org.json.JSONObject;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class House {
    private int area, basePrice, rentPrice, sellPrice;
    private String id, buildingType, address, imageURL, phone, description;
    private Long expireTime;
    private int dealType;
    public House(String id, String buildingType, String address, String imageURL, String phone,
                 String description, Long expireTime, int area, int basePrice, int rentPrice,
                 int sellPrice, int dealType) {
        this.id = id;
        this.buildingType = buildingType;
        this.address = address;
        this.imageURL = imageURL;
        this.phone = phone;
        this.description = description;
        this.expireTime = expireTime;
        this.area = area;
        this.basePrice = basePrice;
        this.rentPrice = rentPrice;
        this.sellPrice = sellPrice;
        this.dealType = dealType;
    }

    public String toJSONString(){
        JSONObject jsonObject = new JSONObject(this);
        return jsonObject.toString();
    }

    public JSONObject toJSON(){
        return new JSONObject(this);
    }

    static House createHouseFromJSON(JSONObject house){
        String id = house.getString("id");
        int area = house.getInt("area");
        String address = house.getString("address");
        int dealType = house.getInt("dealType");
        String phone = house.getString("phone");
        String description = house.getString("description");
        String imageURL = house.getString("imageURL");
        int rentPrice = 0;
        int sellPrice = 0;
        int basePrice = 0;

        if (dealType == 1 )
            rentPrice = house.getJSONObject("price").getInt("rentPrice");
        else if (dealType == 0)
            sellPrice = house.getJSONObject("price").getInt("sellPrice");

        String buildingType = "";
        if(house.getString("buildingType").equals("ویلایی"))
            buildingType += "villa";
        else if(house.getString("buildingType").equals("آپارتمان"))
            buildingType+= "apartment";
        return new House(id,buildingType,address,imageURL,phone,description,null,
                area,basePrice,rentPrice,sellPrice,dealType);
    }

    static House createHouseFromJSONAndTimeStamp(JSONObject house, Long expireTime){
        String id = house.getString("id");
        int area = house.getInt("area");
        String address = house.getString("address");
        int dealType = house.getInt("dealType");
        String imageURL = house.getString("imageURL");
//        String phone = house.getString("phone");
        int rentPrice = 0;
        int sellPrice = 0;
        int basePrice = 0;
        if (dealType == 1 ) {
            rentPrice = house.getJSONObject("price").getInt("rentPrice");
            basePrice = house.getJSONObject("price").getInt("basePrice");
        }
        else if (dealType == 0)
            sellPrice = house.getJSONObject("price").getInt("sellPrice");
        String buildingType = "";
        if(house.getString("buildingType").equals("ویلایی"))
            buildingType += "villa";
        else if(house.getString("buildingType").equals("آپارتمان"))
            buildingType+= "apartment";
        return new House(id,buildingType,address,imageURL,null,null,expireTime,
                area,basePrice,rentPrice,sellPrice,dealType);
    }

    //public enum DealType {rent, sale}

    public String getDealTypeString(){
        if(this.dealType==0)
            return "فروش";
        else
            return "رهن و اجاره";
    }

    public int getArea() {
        return area;
    }

    public int getBasePrice() {
        return basePrice;
    }

    public int getRentPrice() {
        return rentPrice;
    }

    public int getSellPrice() {
        return sellPrice;
    }

    public String getId() {
        return id;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public String getPersianBuildingType() {
        if(this.getBuildingType().equals("apartment"))
            return "آپارتمان";
        else if (this.getBuildingType().equals("villa"))
            return "ویلایی";
        return null;
    }

    public String getAddress() {
        return address;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getPhone() {
        return phone;
    }

    public String getDescription() {
        return description;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public int getDealType() {
        return dealType;
    }
}
