package com.ie.khanebedoosh.data;

import com.ie.khanebedoosh.domain.Individual;
import org.omg.PortableInterceptor.INACTIVE;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.ie.khanebedoosh.data.JDBCDriver.DB_URL;

public class HouseIDs {
    public static void addHouseID(String individualUsername, String houseID) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        logger.info("adding houseID: " + houseID + " for "+ individualUsername);
        Connection connection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        try {
            connection = DriverManager.getConnection(DB_URL);
            ps1 = connection.prepareStatement("SELECT * FROM HouseIDs WHERE houseID LIKE ? AND IndividualUsername LIKE ?;");
            ps1.setString(1, houseID);
            ps1.setString(2, individualUsername);
            ps2 = connection.prepareStatement("INSERT INTO HouseIDs (houseID,IndividualUsername) " +
                    "VALUES (?,?);");
            ps2.setString(1, houseID);
            ps2.setString(2, individualUsername);
            ResultSet resultSet = ps1.executeQuery();
            if (!resultSet.next()) {
                ps2.executeUpdate();
            }
        } catch (SQLException ex) {
            logger.info("Error: unable to add House ID");
            ex.printStackTrace();
        } finally {
            try {
                if (ps2 != null)
                    ps2.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            JDBCDriver.wrapUpDBConnection(connection, ps1);
        }
    }

    public static ArrayList<String> getHouseIDs(String individualUsername) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        PreparedStatement ps = null;
        Connection connection = null;
        ArrayList<String> HouseIDs = new ArrayList<String>();
        try {
            connection = DriverManager.getConnection(DB_URL);
            ps = connection.prepareStatement("SELECT * FROM HouseIDs WHERE IndividualUsername LIKE ?;");
            ps.setString(1, individualUsername);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                HouseIDs.add(resultSet.getString("houseID"));
                System.out.println("user "+ individualUsername + " houseID "+ resultSet.getString("houseID"));
            }
        } catch (SQLException ex) {
            logger.info("Error: unable to get HouseIDs " + ex.getMessage() );
            ex.printStackTrace();
        } finally {
            JDBCDriver.wrapUpDBConnection(connection, ps);
        }
        return HouseIDs;
    }
}

