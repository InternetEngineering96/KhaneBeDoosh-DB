package com.ie.khanebedoosh.controller;

import com.ie.khanebedoosh.domain.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

import static com.ie.khanebedoosh.domain.Utility.xssProtector;

@WebServlet("/login")
public class authenticationController extends HttpServlet {
//    public void doOptions(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        //The following are CORS headers. Max age informs the
//        //browser to keep the results of this call for 1 day.
//        resp.setHeader("Access-Control-Allow-Origin", "*");
//        resp.setHeader("Access-Control-Allow-Methods", "GET, POST");
//        resp.setHeader("Access-Control-Allow-Headers", "Content-Type");
//        resp.setHeader("Access-Control-Max-Age", "86400");
//        //Tell the browser what requests we allow.
//        resp.setHeader("Allow", "GET, HEAD, POST, TRACE, OPTIONS");
//    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        JSONObject json = new JSONObject();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        try {
            if (!request.getRequestURI().equals("/login"))
                throw new IllegalArgumentException();
            String jwt = Authentication.issueJWT(xssProtector(request.getParameter("username")), xssProtector(request.getParameter("password")));
            if (jwt != null) {
                response.setStatus(HttpServletResponse.SC_OK);
                json.put("access_token", jwt);
            }
            else {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            }
        } catch (IllegalArgumentException ex) {
            json.put("msg", "Illegal Argument in your request!");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        response.getWriter().write(json.toString());
    }
}