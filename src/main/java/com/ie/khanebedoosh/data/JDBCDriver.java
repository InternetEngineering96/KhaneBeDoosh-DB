package com.ie.khanebedoosh.data;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JDBCDriver {
    private static JDBCDriver jdbcDriver;
    private static final String JDBC_DRIVER = "org.sqlite.JDBC";
//    static final String DB_URL = "jdbc:sqlite:/Users/nicky/khanebedoosh.db";
    static final String DB_URL = "jdbc:sqlite:C:\\Users\\Ardavan\\Documents\\University\\Semester 8\\Internet Engineering\\Computer Assignments\\CA4\\khanebedoosh.db";
    private static Connection connection = null;

    public JDBCDriver() {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        Statement statement = null;
        try {
            Class.forName(JDBC_DRIVER).newInstance();
            logger.info("Connecting to database...");
            connection = DriverManager.getConnection(DB_URL);
            statement = connection.createStatement();
            addTables(statement, logger);
        } catch (ClassNotFoundException ex) {
            System.out.println("Error: unable to load driver class!");
            System.exit(1);
        } catch (IllegalAccessException ex) {
            System.out.println("Error: access problem while loading!");
            System.exit(2);
        } catch (InstantiationException ex) {
            System.out.println("Error: unable to instantiate driver!");
            System.exit(3);
        } catch (SQLException e) {
            System.out.println("Error: unable to get connection");
            System.out.println(e.getMessage());
        } finally {
            wrapUpDBConnection(connection, statement);
        }
    }

    public static JDBCDriver getInstance() {
        if (jdbcDriver == null) {
            jdbcDriver = new JDBCDriver();
        }
        return jdbcDriver;
    }

    static void wrapUpDBConnection(Connection connection, Statement statement) {
        try {
            if (statement != null)
                statement.close();
            if (connection != null)
                connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    static void wrapUpDBConnection(Connection connection, PreparedStatement ps) {
        try {
            if (ps != null)
                ps.close();
            if (connection != null)
                connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void addTables(Statement statement, Logger logger) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS Individuals (" +
                "Name CHAR," +
                "Username CHAR," +
                "Password CHAR," +
                "Phone CHAR," +
                "Balance INT," +
                "isAdmin BIT," +
                "PRIMARY KEY (Username));";
        statement.executeUpdate(sql);
//        logger.info("Individuals Table is Ready");

        sql = "CREATE TABLE IF NOT EXISTS Houses (" +
                "imageURL CHAR," +
                "address CHAR," +
                "buildingType CHAR," +
                "id CHAR NOT NULL ," +
                "dealType INT ," +
                "rentPrice INTEGER ," +
                "basePrice INTEGER ," +
                "expireTime INTEGER ," +
                "description CHAR," +
                "area INTEGER ," +
                "phone CHAR," +
                "sellPrice INT," +
                "PRIMARY KEY (id));";
        statement.executeUpdate(sql);
//        logger.info("Houses Table is Ready");

        sql = "CREATE TABLE IF NOT EXISTS RealStates (" +
                "Name CHAR," +
                "url CHAR," +
                "isAdmin BIT," +
                "PRIMARY KEY (url));";
        statement.executeUpdate(sql);
//        logger.info("RealStates Table is Ready");


        sql = "CREATE TABLE IF NOT EXISTS HouseIDs (" +
                "houseID CHAR ," +
                "IndividualUsername CHAR," +
                "FOREIGN KEY (IndividualUsername) REFERENCES Individuals(Username) " +
                "ON DELETE CASCADE " +
                ");";
        statement.executeUpdate(sql);
//        logger.info("HouseIDs Table is Ready");
    }

    public static Connection getConnection() {
        return connection;
    }
}
