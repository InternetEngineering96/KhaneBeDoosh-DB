package com.ie.khanebedoosh.domain;


public class User {
    public User(String name, boolean isAdmin) {
        this.name = name;
        this.isAdmin = isAdmin;
    }

    public String getName() {
        return name;
    }

    public boolean getIsAdmin() { return isAdmin; }

    private String name;
    private boolean isAdmin;
}
