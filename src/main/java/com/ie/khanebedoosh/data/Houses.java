package com.ie.khanebedoosh.data;

import com.ie.khanebedoosh.domain.House;
import com.sun.xml.internal.bind.v2.model.core.ID;
import org.sqlite.core.DB;
import sun.security.ssl.HandshakeOutStream;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.ie.khanebedoosh.data.JDBCDriver.DB_URL;

public class Houses {

    public static void addHouse(com.ie.khanebedoosh.domain.House House) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        Connection connection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        try {
            connection = DriverManager.getConnection(DB_URL);
            ps1 = connection.prepareStatement("SELECT * FROM Houses WHERE id LIKE ? ;");
            ps1.setString(1, House.getId());
            ResultSet resultSet = ps1.executeQuery();
            ps2 = connection.prepareStatement("INSERT INTO Houses (imageURL,address,buildingType,id,dealType,rentPrice,basePrice," +
                    "expireTime,description,area,phone,sellPrice) " +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?);");
            if (House.getExpireTime() == null) {
                ps2.setString(1, "null");
            } else
                ps2.setString(1, House.getImageURL());
            ps2.setString(2, House.getAddress());
            ps2.setString(3, House.getBuildingType());
            ps2.setString(4, House.getId());
            ps2.setInt(5, House.getDealType());
            ps2.setInt(6, House.getRentPrice());
            ps2.setInt(7, House.getBasePrice());
            if (House.getExpireTime() == null)
                ps2.setString(8, "null");
            else
                ps2.setLong(8, House.getExpireTime());
            ps2.setString(9, House.getDescription());
            ps2.setInt(10, House.getArea());
            ps2.setString(11, House.getPhone());
            ps2.setInt(12, House.getSellPrice());
            if (!resultSet.next()) {
                ps2.executeUpdate();
            }
        } catch (SQLException ex) {
            logger.info("Error: unable to add House" + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (ps2 != null)
                    ps2.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            JDBCDriver.wrapUpDBConnection(connection, ps1);
        }
    }

    public static ArrayList<com.ie.khanebedoosh.domain.House> getHouses() {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        String sql1 = "SELECT * FROM Houses";
        Connection connection = null;
        Statement statement = null;
        ArrayList<com.ie.khanebedoosh.domain.House> Houses = new ArrayList<House>();
        try {
            connection = DriverManager.getConnection(DB_URL);
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql1);
            while (resultSet.next()) {
                Houses.add(new com.ie.khanebedoosh.domain.House(resultSet.getString("id"),
                        resultSet.getString("buildingType"), resultSet.getString("address"),
                        resultSet.getString("imageURL"), resultSet.getString("phone"),
                        resultSet.getString("description"), resultSet.getLong("expireTime"),
                        resultSet.getInt("area"), resultSet.getInt("basePrice"),
                        resultSet.getInt("rentPrice"), resultSet.getInt("sellPrice"),
                        resultSet.getInt("dealType")
                ));

            }
        } catch (SQLException ex) {
            logger.info("Error: unable to get Houses");
            ex.printStackTrace();
        } finally {
            JDBCDriver.wrapUpDBConnection(connection, statement);
        }
        return Houses;
    }

    public static ArrayList<String> getHousesIDs() {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        String sql1 = "SELECT * FROM Houses";
        Connection connection = null;
        Statement statement = null;
        ArrayList<String> IDs = new ArrayList<String>();
        try {
            connection = DriverManager.getConnection(DB_URL);
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql1);
            while (resultSet.next()) {
                IDs.add(resultSet.getString("id"));
            }
        } catch (SQLException ex) {
            logger.info("Error: unable to get Houses IDs");
            ex.printStackTrace();
        } finally {
            JDBCDriver.wrapUpDBConnection(connection, statement);
        }
        return IDs;
    }

    public static com.ie.khanebedoosh.domain.House getHouseById(String id) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        com.ie.khanebedoosh.domain.House House = null;
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DriverManager.getConnection(DB_URL);
            ps = connection.prepareStatement("SELECT * FROM Houses WHERE id LIKE ?;");
            ps.setString(1, id);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                House = new com.ie.khanebedoosh.domain.House(resultSet.getString("id"),
                        resultSet.getString("buildingType"), resultSet.getString("address"),
                        resultSet.getString("imageURL"), resultSet.getString("phone"),
                        resultSet.getString("description"), resultSet.getLong("expireTime"),
                        resultSet.getInt("area"), resultSet.getInt("basePrice"),
                        resultSet.getInt("rentPrice"), resultSet.getInt("sellPrice"),
                        resultSet.getInt("dealType")
                );
            }
        } catch (SQLException ex) {
            logger.info("Error: unable to get House By Given Id");
            ex.printStackTrace();
        } finally {
            JDBCDriver.wrapUpDBConnection(connection, ps);
        }
        return House;
    }

    public static boolean doesHouseExist(String id) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DriverManager.getConnection(DB_URL);
            ps = connection.prepareStatement("SELECT * FROM Houses WHERE id LIKE ?;");
            ps.setString(1, id);
            ResultSet resultSet = ps.executeQuery();
            return resultSet.next();
        } catch (SQLException ex) {
            logger.info("Error: unable to get House By Given Id");
            ex.printStackTrace();
        } finally {
            JDBCDriver.wrapUpDBConnection(connection, ps);
        }
        return false;
    }

    public static boolean removeExpiredHousesIfExist() {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        Connection connection = null;
        PreparedStatement ps = null;
        int rowCount = 0;
        try {
            connection = DriverManager.getConnection(DB_URL);
            ps = connection.prepareStatement("DELETE FROM Houses WHERE expireTime BETWEEN 1 AND ?;");
            ps.setLong(1, (System.currentTimeMillis()) - 1L);
            rowCount = ps.executeUpdate();
            logger.info("Number of deleted rows: " + rowCount);
        } catch (SQLException ex) {
            logger.info("Error: unable to remove expired houses");
            ex.printStackTrace();
        } finally {
            JDBCDriver.wrapUpDBConnection(connection, ps);
        }
        return (rowCount > 0);
    }

    public static ArrayList<com.ie.khanebedoosh.domain.House> getHouseByQuery(com.ie.khanebedoosh.domain.Search query) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        int minArea = 0;
        int dealType1, dealType2, rentPrice, sellPrice;
        String buildingType1, buildingType2;
        if (query.getMinArea() != null)
            minArea = query.getMinArea();

        if (query.getDealType() == null) {
            dealType1 = 0;
            dealType2 = 1;
        } else {
            dealType1 = query.getDealType();
            dealType2 = query.getDealType();
        }

        if (query.getBuildingType() == null) {
            buildingType1 = "villa";
            buildingType2 = "apartment";
        } else {
            buildingType1 = query.getBuildingType();
            buildingType2 = query.getBuildingType();
        }

        String sql1 = "SELECT * FROM Houses WHERE (id IS NOT NULL) AND (area >= ? ) " +
                "AND (dealType = ? OR dealType = ?) AND (buildingType = ? OR buildingType = ?) AND ((sellPrice <= ?) AND (rentPrice <= ?));";
        String sql2 = "SELECT * FROM Houses WHERE (id IS NOT NULL) AND (area >= ? ) " +
                "AND (dealType = ? OR dealType = ?) AND (buildingType = ? OR buildingType = ?) AND (rentPrice <= ?);";
        String sql3 = "SELECT * FROM Houses WHERE (id IS NOT NULL) AND (area >= ? ) " +
                "AND (dealType = ? OR dealType = ?) AND (buildingType = ? OR buildingType = ?) AND (sellPrice <= ?);";
        String sql4 = "SELECT * FROM Houses WHERE (id IS NOT NULL) AND (area >= ? ) " +
                "AND (dealType = ? OR dealType = ?) AND (buildingType = ? OR buildingType = ?);";

//        String sql1 = "SELECT * FROM Houses WHERE (id IS NOT NULL)" +
//                (query.getMinArea() == null ? "" : " AND (area >= " + query.getMinArea() + ")") +
//                (query.getDealType() == null ? "" : " AND (dealType = " + query.getDealType() + ")") +
//                (query.getBuildingType() == null ? "" : " AND (buildingType LIKE '" + query.getBuildingType() + "')") +
//                (query.getDealType() == null
//                        ? ((query.getMaxRentPrice() != null) ? " AND ((rentPrice <= " + query.getMaxRentPrice() + ") OR rentPrice = NULL)" : "")
//                        : (query.getDealType() == 1
//                        ? (query.getMaxRentPrice() != null ? " AND (rentPrice <= " + query.getMaxRentPrice() + ")" : "")
//                        : (query.getMaxSellPrice() != null ? " AND (sellPrice <= " + query.getMaxSellPrice() + ")" : ""))) + ";";

        ArrayList<com.ie.khanebedoosh.domain.House> Houses = new ArrayList<House>();
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DriverManager.getConnection(DB_URL);
            if (query.getDealType() == null) {
                if (query.getMaxRentPrice() != null) {
                    rentPrice = query.getMaxRentPrice();
                    sellPrice = query.getMaxSellPrice();
                    ps = connection.prepareStatement(sql1);
                    ps.setInt(6, sellPrice);
                    ps.setInt(7, rentPrice);
                } else
                    ps = connection.prepareStatement(sql4);
            } else {
                if (query.getDealType() == 1) {
                    //rent
                    if (query.getMaxRentPrice() != null) {
                        //rentPrice is specified
                        rentPrice = query.getMaxRentPrice();
                        ps = connection.prepareStatement(sql2);
                        ps.setInt(6, rentPrice);
                    } else
                        ps = connection.prepareStatement(sql4);
                } else {
                    //sell
                    if (query.getMaxSellPrice() != null) {
                        //sellPrice is specified
                        sellPrice = query.getMaxSellPrice();
                        ps = connection.prepareStatement(sql3);
                        ps.setInt(6, sellPrice);
                    } else
                        ps = connection.prepareStatement(sql4);
                }
            }
            ps.setInt(1, minArea);
            ps.setInt(2, dealType1);
            ps.setInt(3, dealType2);
            ps.setString(4, buildingType1);
            ps.setString(5, buildingType2);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Houses.add(new com.ie.khanebedoosh.domain.House(resultSet.getString("id"),
                        resultSet.getString("buildingType"), resultSet.getString("address"),
                        resultSet.getString("imageURL"), resultSet.getString("phone"),
                        resultSet.getString("description"), resultSet.getLong("expireTime"),
                        resultSet.getInt("area"), resultSet.getInt("basePrice"),
                        resultSet.getInt("rentPrice"), resultSet.getInt("sellPrice"),
                        resultSet.getInt("dealType")
                ));
            }
        } catch (SQLException ex) {
            System.out.println("Error: unable to get House By Given Query " + ex.getMessage());
        } finally {
            JDBCDriver.wrapUpDBConnection(connection, ps);
        }
        return Houses;
    }

}
