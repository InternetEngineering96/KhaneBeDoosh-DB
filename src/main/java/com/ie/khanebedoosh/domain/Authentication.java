package com.ie.khanebedoosh.domain;

import com.ie.khanebedoosh.data.Individuals;
import io.jsonwebtoken.*;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Authentication {
    static private SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
    static private byte[] apiKeySecretBytes = ("mozi-amoo").getBytes();
    static private Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

    private static String createJWT(String issuer, String subject) {
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        JwtBuilder builder = Jwts.builder()
                .setIssuedAt(now)
                .setSubject(subject)
                .setIssuer(issuer)
                .signWith(signatureAlgorithm, signingKey);
        return builder.compact();
    }

    public static boolean checkValidity(String jwt) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        try {
            Claims claims = Jwts.parser().setSigningKey(signingKey).parseClaimsJws(jwt).getBody();
//            logger.info("Subject: " + claims.getSubject());
//            logger.info("Issuer: " + claims.getIssuer());
//            logger.info("Expiration: " + claims.getExpiration());
//            Header header = Jwts.parser().setSigningKey(signingKey).parseClaimsJws(jwt).getHeader();
//            logger.info("Algorithm: " + ((JwsHeader) header).getAlgorithm());
            return true;
        } catch (SignatureException ex) {
            return false;
        }
    }

    public static Individual getSubject(String jwt) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        try {
            Claims claims = Jwts.parser().setSigningKey(signingKey).parseClaimsJws(jwt).getBody();
            logger.info("Subject: " + claims.getSubject());
            return Individuals.getIndividual(claims.getSubject());
        } catch (SignatureException ex) {
            return null;
        }
    }

    public static String issueJWT(String username, String password) {
        String issuer = "Khee server!";
        if (Individuals.checkCredentials(username, password))
            return createJWT(issuer, username);
        else
            return null;
    }

}
