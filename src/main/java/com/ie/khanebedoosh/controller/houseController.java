package com.ie.khanebedoosh.controller;

import com.ie.khanebedoosh.data.Individuals;
import com.ie.khanebedoosh.domain.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

import static com.ie.khanebedoosh.domain.Search.getRequestedHousesFromAllUsers;
import static com.ie.khanebedoosh.domain.Utility.xssProtector;

@WebServlet("/houses/*")
public class houseController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        JSONObject json = new JSONObject();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        Individual individual = (Individual) request.getAttribute("user");
        try {
            if (!request.getRequestURI().equals("/houses"))
                throw new IllegalArgumentException();
            Utility.addHouseFromRequest(individual, request);
            response.setStatus(HttpServletResponse.SC_CREATED);
        } catch (IllegalArgumentException ex) {
            json.put("msg", "Illegal Argument in your request!");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        response.getWriter().write(json.toString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        JSONObject json = new JSONObject();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
//        Individual individual = (Individual) request.getAttribute("user");
        Individual individual = Individuals.getIndividual(((Individual)request.getAttribute("user")).getUsername());
        if (request.getRequestURI().equals("/houses"))
            search(request, response, logger, json);
        else {
            String urlAfterContext = request.getPathInfo().substring(1);
            if (urlAfterContext.equals("bought")) {
                getBoughtHouses(request, response, logger, json, individual);
            } else if (urlAfterContext.contains("/")) {
                purchasePhoneNumber(request, response, logger, json, individual, urlAfterContext);
            } else
                getHouseInfo(request, response, logger, json, urlAfterContext);
        }
    }

    private void getBoughtHouses(HttpServletRequest request, HttpServletResponse response, Logger logger, JSONObject json, Individual individual) throws IOException {
        try {
            //todo
            System.out.println("Admin stat: " + individual.getIsAdmin());
            if (!individual.getIsAdmin()) {
                json.put("boughtHouses", new JSONArray(individual.getBoughtHouseIDs()));
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                for (Individual individual1 : Manager.getIndividuals()) {
                    System.out.println("individual name is: " + individual1.getUsername());
                    json.put(individual1.getUsername(), new JSONArray(individual1.getBoughtHouseIDs()));
                }
                response.setStatus(HttpServletResponse.SC_OK);
            }
        } catch (Exception ex) {
            request.setAttribute("msg", "Error in retrieving bought houses! Try Again!");
            logger.warning(ex.getMessage() + " Error in retrieving bought houses!!!");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        response.getWriter().write(json.toString());
    }

    private void purchasePhoneNumber(HttpServletRequest request, HttpServletResponse response, Logger logger, JSONObject json, Individual individual, String urlAfterContext) throws IOException {
        try {
            String restOfURL = urlAfterContext.substring(urlAfterContext.indexOf('/') + 1);
            String houseID = urlAfterContext.substring(0, urlAfterContext.indexOf('/'));
            logger.info("Bought house phone No. with id " + houseID + " for user " + individual.getUsername());
            if (restOfURL.equals("phone")) {
                if (individual.getBalance() >= 1000 && !individual.isPhoneNumBought(houseID)) {
                    json.put("purchaseSuccessStatus", "true");
                    individual.addBoughtHouseID(houseID);
                    individual.decreaseBalance();
                } else if (individual.getBalance() < 1000 && !individual.isPhoneNumBought(houseID)) {
                    json.put("purchaseSuccessStatus", "false");
                } else if (individual.isPhoneNumBought(houseID)) {
                    json.put("purchaseSuccessStatus", "true");
                }
                response.setStatus(HttpServletResponse.SC_ACCEPTED);
            } else throw new IllegalArgumentException();
        } catch (Exception ex) {
            request.setAttribute("msg", "Error in purchasing phone number! Try Again!");
            logger.warning(ex.getMessage() + " Error in purchasing phone number of house with specified id!!!");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        response.getWriter().write(json.toString());
    }

    private void getHouseInfo(HttpServletRequest request, HttpServletResponse response, Logger logger, JSONObject json, String houseID) throws IOException {
        try {
            RealState realState = Manager.getRealStates().get(0);
//            House house = Utility.findHouseByID(houseID, individual, realState);
            House house = Utility.findHouseByID(houseID);
            json.put("house", house.toJSON());
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception ex) {
            request.setAttribute("msg", "Error in finding the house! Try Again!");
            logger.warning(ex.getMessage() + " Error in finding house with specified id!!!");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        response.getWriter().write(json.toString());
    }

    private void search(HttpServletRequest request, HttpServletResponse response, Logger logger, JSONObject json) throws IOException {
        try {
            Search query = new Search(xssProtector(request.getParameter("minArea")), xssProtector(request.getParameter("buildingType")),
                    xssProtector(request.getParameter("dealType")), xssProtector(request.getParameter("maxPrice")));

            ArrayList<House> requestedHouses = getRequestedHousesFromAllUsers(query);
            JSONArray jsonArray = new JSONArray(requestedHouses);
            json.put("houses", jsonArray);
            System.out.println(json.toString());
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception ex) {
            request.setAttribute("msg", "Error in finding houses! Try Again!");
            logger.warning(ex.getMessage() + " Error in house controller search method!!!");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

        }
        response.getWriter().write(json.toString());
    }
}
