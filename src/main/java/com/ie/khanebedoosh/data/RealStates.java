package com.ie.khanebedoosh.data;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.ie.khanebedoosh.data.JDBCDriver.DB_URL;

public class RealStates {
    public static void addRealState(String name, String url, Boolean isAdmin) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        logger.info("adding RealState: " + url);
        Connection connection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        try {
            connection = DriverManager.getConnection(DB_URL);
            ps1 = connection.prepareStatement("SELECT * FROM RealStates WHERE url LIKE ?;");
            ps1.setString(1, url);
            ps2 = connection.prepareStatement("INSERT INTO RealStates (Name,url,isAdmin) VALUES (?,?,?);");
            ps2.setString(1, name);
            ps2.setString(2, url);
            ps2.setBoolean(3, isAdmin);
            ResultSet resultSet = ps1.executeQuery();
            if (!resultSet.next())
                ps2.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: unable to add RealState " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (ps2 != null)
                    ps2.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            JDBCDriver.wrapUpDBConnection(connection, ps1);
        }
    }

    public static List<com.ie.khanebedoosh.domain.RealState> getRealStates() {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        String sql1 = "SELECT * FROM RealStates";
        Connection connection = null;
        Statement statement = null;
        ArrayList<com.ie.khanebedoosh.domain.RealState> RealStates = new ArrayList<com.ie.khanebedoosh.domain.RealState>();
        try {
            connection = DriverManager.getConnection(DB_URL);
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql1);
            while (resultSet.next()) {
                RealStates.add(new com.ie.khanebedoosh.domain.RealState(resultSet.getString("Name"),
                        resultSet.getString("url"), resultSet.getBoolean("isAdmin")));
            }
        } catch (SQLException ex) {
            System.out.println("Error: unable to get RealStates");
            ex.printStackTrace();
        } finally {
            JDBCDriver.wrapUpDBConnection(connection, statement);
        }
        return RealStates;
    }
}
