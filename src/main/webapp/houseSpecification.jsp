<%@ page import="static com.ie.khanebedoosh.domain.Utility.findHouseByID" %>
<%@ page import="com.ie.khanebedoosh.*" contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ie.khanebedoosh.domain.*" %>
<%--
  Created by IntelliJ IDEA.
  User: nicky
  Date: 2/16/18
  Time: 10:16 PM
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <title>مشخصات کامل خانه</title>
    <style>table, th, td {
        border: 1px solid black;
    }</style>
</head>
<body>
<%
    Individual individual = Manager.getIndividuals().get(0);
    RealState realState = Manager.getRealStates().get(0);
    String id = (String) request.getParameter("id");
    House house = Utility.findHouseByID(id, individual, realState);
%>
<p align="right"> نام کاربری: <%=individual.getName()%>
</p>
<p align="right"> اعتبار شما: <%=individual.getBalance()%>
</p>
<br>
<table style="width:100%" align="right" dir="rtl">
    <tr>
        <td> نوع ساختمان:<%=house.getPersianBuildingType()%>
        </td>
        <td> متراژ: <%= house.getArea() %>
        </td>
    </tr>
    <tr>
        <td>نوع قرارداد: <%=house.getDealTypeString()%>
        </td>
        <% if (house.getDealType() == 1) { %>
        <td> قیمت پایه:   <%= house.getBasePrice() %>
        </td>
        <% } else if (house.getDealType() == 0) { %>
        <td> قیمت خرید:   <%= house.getSellPrice() %>
        </td>
        <% } %>
    </tr>
    <tr>
        <% if (house.getDealType() == 1) { %>
        <td> قیمت اجاره:   <%= house.getRentPrice() %>
        </td>
        <% } %>
        <%if (house.getAddress() != null) {%>
        <td> آدرس:   <%= house.getAddress() %>
        </td>
        <% } else { %>
        <td> آدرس: ندارد</td>
        <% } %>
    </tr>
    <tr>
        <%if (house.getImageURL() != null) {%>
        <td> لینک عکس:   <%= house.getImageURL() %>
        </td>
        <% } else { %>
        <td> لینک عکس: ندارد</td>
        <% } %>
        <%if (house.getDescription() != null) {%>
        <td> توضیحات:   <%= house.getDescription() %>
        </td>
        <% } else { %>
        <td> توضیحات: ندارد</td>
        <% } %>
    </tr>
</table>
<br>
<%
    if (request.getAttribute("Searched") == null) {
%>
<form action="phone.jsp" method="POST" align="right">
    <input type="hidden" name="id" value="<%=house.getId()%>" align="right">
    <input type="submit" value="دریافت شماره مالک/مشاور" align="right"/>
</form>
<form action="index.jsp" method="POST" align="right">
    <input type="submit" value="بازگشت به صفحه‌ی اصلی" align="right"/>
</form>
<%
    }
    else if (individual.isPhoneNumBought(house.getId())) {
%>
<br>
<p align="right"><%= house.getPhone() %>
</p>
<form action="index.jsp" method="POST" align="right">
    <input type="submit" value="بازگشت به صفحه اصلی" align="right"/>
</form>
<%
    }  if (request.getAttribute("fail") != null && !individual.isPhoneNumBought(house.getId())) {
%>
<p align="right">موجودی شما برای دریافت شماره‌ی مالک/مشاور کافی نیست</p>
<form action="index.jsp" method="POST" align="right">
    <input type="submit" value="بازگشت به صفحه‌ی اصلی جهت افزایش اعتبار" align="right"/>
</form>
<%
    }
%>
</body>
</html>
