package com.ie.khanebedoosh.controller;

import com.ie.khanebedoosh.domain.*;
import org.json.JSONObject;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

import static com.ie.khanebedoosh.domain.Utility.xssProtector;

@WebServlet("/users/*")
public class userController extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        JSONObject json = new JSONObject();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        Individual individual = (Individual) request.getAttribute("user");
        try {
            if (request.getRequestURI().equals("/users/")) {
                System.out.println("query info: " + xssProtector(request.getQueryString()));
                if (xssProtector(request.getQueryString()) == null) {
                    json.put("individual", individual.toJSON());

                } else if (request.getParameterMap().containsKey("houseID")) {
                    json.put("hasPayed", individual.isPhoneNumBought(xssProtector(request.getParameter("houseID"))));
                } else throw new IllegalArgumentException();
                response.setStatus(HttpServletResponse.SC_OK);
            } else throw new IllegalArgumentException();
        } catch (Exception ex) {
            json.put("msg", "Illegal Argument in your request!");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        response.getWriter().write(json.toString());
    }
}
