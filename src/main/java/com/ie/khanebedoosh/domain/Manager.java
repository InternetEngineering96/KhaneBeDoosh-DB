package com.ie.khanebedoosh.domain;

import com.ie.khanebedoosh.data.*;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.ie.khanebedoosh.domain.Utility.xssProtector;

public class Manager {

    public Manager() {
    }

    private static JDBCDriver jdbcDriver;
    private static List<RealState> realStates;

    static {
        realStates = new ArrayList<RealState>();
        jdbcDriver = JDBCDriver.getInstance();
        Individuals.addIndividual(new Individual("بهنام همایون", null,
                0, "behnamhomayoon", "password", true));
        realStates.add(new RealState("KhaneBeDoosh", "http://139.59.151.5:6664/khaneBeDoosh/v2/house", false));
        Utility.syncDatabase();
        
    }

    public static List<Individual> getIndividuals() {
        return Individuals.getIndividuals();
    }

    public static List<RealState> getRealStates() {
        return realStates;
    }

    public static JSONObject getAllHouses(RealState realState) throws IOException {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        return realState.getAllHouses();
    }
}
