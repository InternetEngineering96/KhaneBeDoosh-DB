package com.ie.khanebedoosh.controller;

import com.ie.khanebedoosh.data.Individuals;
import com.ie.khanebedoosh.domain.Authentication;
import io.jsonwebtoken.JwtException;

import javax.security.auth.login.LoginException;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.ie.khanebedoosh.domain.Utility.xssProtector;

@WebFilter(
        urlPatterns = {"/users/*", "/houses/*", "/balance"}
)
public class AuthenticationFilter implements Filter {
    private static final java.util.logging.Logger LOG = Logger.getLogger(AuthenticationFilter.class.getName());
    private static final String AUTH_HEADER_KEY = "Authorization";
    private static final String AUTH_HEADER_VALUE_PREFIX = "Bearer ";

    public void init(FilterConfig filterConfig) {
        LOG.info("AuthenticationFilter initialized");
    }

    public void destroy() {
        LOG.info("AuthenticationFilter destroyed");
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (request.getMethod().equals("OPTIONS")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        try {
            String jwt = xssProtector(getBearerToken(request));
            System.out.println("JWT is: " + jwt);
            if (jwt != null && !jwt.equals("")) {
                if (!Authentication.checkValidity(jwt))
                    throw new LoginException();
                LOG.info("Logged in using JWT");
                request.setAttribute("user", Authentication.getSubject(jwt));
            } else {
                if (request.getRequestURI().equals("/balance") || (request.getRequestURI().equals("/users/")) || (request.getRequestURI().equals("/users")) ||(request.getPathInfo() != null && request.getPathInfo().substring(1).endsWith("/phone")))
                    throw new LoginException();
                else {
                    LOG.info("No JWT provided, go on unauthenticated");
                    request.setAttribute("user", null);
                }
            }
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (final LoginException e) {
            LOG.log(Level.WARNING, "Failed logging in with security token", e);
            response.setContentLength(0);
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    private String getBearerToken(HttpServletRequest request) {
        String authHeader = xssProtector(request.getHeader(AUTH_HEADER_KEY));
        if (authHeader != null && authHeader.startsWith(AUTH_HEADER_VALUE_PREFIX)) {
            return authHeader.substring(AUTH_HEADER_VALUE_PREFIX.length());
        }
        return null;
    }
}
