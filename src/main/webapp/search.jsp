<%--
  Created by IntelliJ IDEA.
  User: Ardavan
  Date: 16/02/2018
  Time: 12:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.ie.khanebedoosh.*" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="java.util.logging.Level" %>
<%@ page import="com.ie.khanebedoosh.domain.Search" %>
<%@ page import="static com.ie.khanebedoosh.domain.Search.findRealStateHouseIDs" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.ie.khanebedoosh.domain.House" %>
<%@ page import="static com.ie.khanebedoosh.domain.Search.getRequestedHousesFromAllUsers" %>
<%@ page import="com.ie.khanebedoosh.domain.Individual" %>
<%@ page import="com.ie.khanebedoosh.domain.Manager" %>
<html>
<head>
    <title>Search</title>
</head>
<body>
<% Individual user = (Individual) Manager.getIndividuals().get(0); %>

<p align="right"> نام کاربری: <%=user.getName()%>
</p>
<p align="right"> اعتبار شما: <%=user.getBalance()%>
</p>
<br>
    <%
    Logger logger = Logger.getLogger("com.ie");
    logger.setLevel(Level.INFO);

    try {
        request.setCharacterEncoding("UTF-8");
        Search query = new Search(request.getParameter("minArea"), request.getParameter("buildingType"),
                request.getParameter("dealType"), request.getParameter("maxPrice"));
        ArrayList<House> requestedHouses = getRequestedHousesFromAllUsers(query);
        for (House house : requestedHouses) {
            if (house.getDealType() == 0) {
    %>

        <p align="right">قیمت: <%= house.getSellPrice()%></p>
    <%
        }
        else{
    %>
        <p align="right">قیمت پایه: <%= house.getBasePrice()%> </p>
        <p align="right">مبلغ اجاره: <%= house.getRentPrice()%> </p>
    <%
        }
    %>
        <p align="right">متراژ: <%= house.getArea()%> </p>
        <p align="right">نوع: <%= house.getDealTypeString()%> </p>
    <%
        if (house.getImageURL() != null) {
    %>
        <p align="right" dir="rtl">لینک عکس: <%= house.getImageURL()%> </p>
        <br>
        <div style="text-align: right"><img src = "<%= house.getImageURL()%>" style="width:30%"></div>
        <br>
    <%
        } else{
            String relativeWebPath = "no-pic.jpg";
    %>
        <div style="text-align: right"><img src = "<%= relativeWebPath%>" style="width:30%"></div>
    <%
        }
    %>
        <%--<p align = "right"><a href="houseSpecification.jsp" >اطلاعات بیشتر</a></p>--%>
        <form action="houseSpecification.jsp"  method = "post">
            <input type="hidden" name="id" value="<%=house.getId()%>" align="right">
            <input type="submit" value="اطلاعات بیش‌تر" align="right">
        </form>
        <hr>

<%
        }
    } catch (Exception ex) {
        request.setAttribute("msg", "Error in finding houses! Try Again!");
        logger.warning(ex.getMessage() + " Error!!!");
    %>
        <jsp:forward page="index.jsp"/>
    <%
    }
%>
<br>
<a href="index.jsp"> Home Page </a>
</body>
</html>
