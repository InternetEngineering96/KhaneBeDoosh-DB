package com.ie.khanebedoosh.domain;

import com.ie.khanebedoosh.data.Individuals;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class Individual extends User {

    private String phone;
    private int balance;
    private String username;
    private String password;
//    public static ArrayList<String> boughtHouseIDs = new ArrayList<String>();
    private ArrayList<House> houses = new ArrayList<House>();

    public Individual(String name, String phone, int balance, String username, String password, boolean isAdmin) {
        super(name, isAdmin);
        this.phone = phone;
        this.balance = balance;
        this.username = username;
        this.password = password;
    }

    public JSONObject toJSON(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name",this.getName());
        jsonObject.put("balance",this.getBalance());
        return jsonObject;
    }

    public void addHouse(String id, String buildingType, String address, String imageURL, String phone,
                         String description, Long expireTime, int area, int basePrice, int rentPrice,
                         int sellPrice, int dealType) {
        this.houses.add(new House(id, buildingType, address, imageURL, phone, description, expireTime, area,
                basePrice, rentPrice, sellPrice, dealType));
    }

    public boolean increaseBalance(int value) throws IOException {
        boolean status = Bank.sendPayRequestAndGetResponse(value);
        if (status) {
            balance += value;
            Individuals.setBalance(this, balance);
        }
        return status;
    }
    public void decreaseBalance(){
        balance -= 1000;
        Individuals.setBalance(this, balance);
    }

    public ArrayList<String> getBoughtHouseIDs() {
        return com.ie.khanebedoosh.data.HouseIDs.getHouseIDs(this.getUsername());
    }
    public void addBoughtHouseID(String id){
        com.ie.khanebedoosh.data.HouseIDs.addHouseID(this.getUsername(),id);
//        boughtHouseIDs.add(id);
    }

    public boolean isPhoneNumBought(String id) {
        ArrayList<String> boughtHouseIDs = getBoughtHouseIDs();
        for (int i = 0; i < boughtHouseIDs.size(); i++) {
            if (boughtHouseIDs.get(i).equals(id))
                return true;
        }
        return false;
    }

    public String getPhone() {
        return phone;
    }

    public int getBalance() {
        return balance;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}
