package com.ie.khanebedoosh.data;

import com.ie.khanebedoosh.domain.Individual;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.ie.khanebedoosh.data.JDBCDriver.DB_URL;

public class Individuals {
    public static void addIndividual(com.ie.khanebedoosh.domain.Individual individual) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        logger.info("adding individual: " + individual.getUsername());
        Connection connection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        try {
            connection = DriverManager.getConnection(DB_URL);
            ps1 = connection.prepareStatement("SELECT * FROM Individuals WHERE Username LIKE ?;");
            ps1.setString(1, individual.getUsername());
            ResultSet resultSet = ps1.executeQuery();
            ps2 = connection.prepareStatement("INSERT INTO Individuals (Name,Username,Password,Phone,Balance,isAdmin) VALUES (?,?,?,?,?,?);");
            ps2.setString(1, individual.getName());
            ps2.setString(2, individual.getUsername());
            ps2.setString(3, individual.getPassword());
            ps2.setString(4, individual.getPhone());
            ps2.setInt(5, individual.getBalance());
            ps2.setBoolean(6, individual.getIsAdmin());
            if (!resultSet.next())
                ps2.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: unable to add Individuals");
            ex.printStackTrace();
        } finally {
            try {
                if (ps2 != null)
                    ps2.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            JDBCDriver.wrapUpDBConnection(connection, ps1);
        }
    }

    public static List<com.ie.khanebedoosh.domain.Individual> getIndividuals() {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        String sql1 = "SELECT * FROM Individuals";
        Connection connection = null;
        Statement statement = null;
        ArrayList<com.ie.khanebedoosh.domain.Individual> individuals = new ArrayList<com.ie.khanebedoosh.domain.Individual>();
        try {
            connection = DriverManager.getConnection(DB_URL);
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql1);
            while (resultSet.next()) {
                individuals.add(new Individual(resultSet.getString("Name"),
                        resultSet.getString("Phone"), resultSet.getInt("Balance"),
                        resultSet.getString("Username"), resultSet.getString("Password"), resultSet.getBoolean("isAdmin")));
            }
        } catch (SQLException ex) {
            System.out.println("Error: unable to get Individuals");
            ex.printStackTrace();
        } finally {
            JDBCDriver.wrapUpDBConnection(connection, statement);
        }
        return individuals;
    }

    //assuming username is unique
    public static Individual getIndividual(String username) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        Connection connection = null;
        PreparedStatement ps = null;
        Individual individual = null;
        try {
            connection = DriverManager.getConnection(DB_URL);
            ps = connection.prepareStatement("SELECT * FROM Individuals WHERE Username LIKE ? ;");
            ps.setString(1, username);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                individual = new Individual(resultSet.getString("Name"),
                        resultSet.getString("Phone"), resultSet.getInt("Balance"),
                        resultSet.getString("Username"), resultSet.getString("Password"), resultSet.getBoolean("isAdmin"));
            }
        } catch (SQLException ex) {
            System.out.println("Error: unable to get Individuals");
            ex.printStackTrace();
        } finally {
            JDBCDriver.wrapUpDBConnection(connection, ps);
        }
        return individual;
    }

    public static void setBalance(com.ie.khanebedoosh.domain.Individual individual, int balance) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DriverManager.getConnection(DB_URL);
            ps = connection.prepareStatement("UPDATE Individuals SET Balance = ? WHERE Username = ? ;");
            ps.setInt(1, balance);
            ps.setString(2, individual.getUsername());
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: unable to increase balance");
            ex.printStackTrace();
        } finally {
            JDBCDriver.wrapUpDBConnection(connection, ps);
        }
    }

    public static boolean checkCredentials(String username, String password) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DriverManager.getConnection(DB_URL);
            ps = connection.prepareStatement("SELECT * FROM Individuals WHERE Username LIKE ? AND Password LIKE ?;");
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            System.out.println("Error: unable to check credentials");
            ex.printStackTrace();
        } finally {
            JDBCDriver.wrapUpDBConnection(connection, ps);
        }
        return false;
    }
}
